
// Listen for form submit
window.onload=function() {
document.getElementById('form-bimbingan').addEventListener('submit', saveBimbingan);

// Save Bookmark
function saveBimbingan(e){
  // Get form values
  var nomorInduk =document.getElementById('nomorInduk').value;
  var nama =document.getElementById('nama').value;
  var kelas =document.getElementById('kelas').value;
  var jurusan =document.getElementById('jurusan').value;
  var judul =document.getElementById('judul').value;
  var tgl =document.getElementById('tgl').value;
  var jam =document.getElementById('jam').value;
  var email =document.getElementById('email').value;
  var materi =document.getElementById('materi').value;

  if(!validateForm(nomorInduk, nama, kelas, jurusan, judul, tgl, jam, email, materi)){
    return false;
  }

  var bimbingan = {
    nim: nomorInduk,
    nama: nama,
    kelas: kelas,
    jurusan: jurusan,
    judulSkripsi : judul,
    tanggalBimbingan: tgl,
    jamBimbingan: jam,
    email: email,
    materiBimbingan: materi
  }
  /*
    // Local Storage Test
    localStorage.setItem('test', 'Hello World');
    console.log(localStorage.getItem('test'));
    localStorage.removeItem('test');
    console.log(localStorage.getItem('test'));
  */

  // Test if bookmarks is null
  if(localStorage.getItem('bimbingans') === null){
    // Init array
    var bimbingans = [];
    // Add to array
    bimbingans.push(bimbingan);
    // Set to localStorage
    localStorage.setItem('bimbingans', JSON.stringify(bimbingans));
  } else {
    // Get bookmarks from localStorage
    var bimbingans = JSON.parse(localStorage.getItem('bimbingans'));
    // Add bookmark to array
    bimbingans.push(bimbingan);
    // Re-set back to localStorage
    localStorage.setItem('bimbingans', JSON.stringify(bimbingans));
  }

  // Clear form
  document.getElementById('form-bimbingan').reset();

  // Re-fetch bookmarks
  fetchBimbingans();

  // Prevent form from submitting
  e.preventDefault();
}

// Delete bookmark
function deleteBimbingan(nomorInduk){
  // Get bookmarks from localStorage
  var bimbingans = JSON.parse(localStorage.getItem('bimbingans'));
  // Loop through the bookmarks
  for(var i =0;i < bimbingans.length;i++){
    if(bimbingans[i].nomorInduk == nomorInduk){
      // Remove from array
      bimbingans.splice(i, 1);
    }
  }
  // Re-set back to localStorage
  localStorage.setItem('bimbingans', JSON.stringify(bimbingans));

  // Re-fetch bookmarks
  fetchBimbingans();
}

// Fetch bookmarks
function fetchBimbingans(){
  // Get bookmarks from localStorage
  var bimbingans = JSON.parse(localStorage.getItem('bimbingans'));
  // Get output id
  var bimbingansResults = document.getElementById('bimbingansResults');

  // Build output
  bimbingansResults.innerHTML = '';
  for(var i = 0; i < bimbingans.length; i++){
    var nomorInduk = bimbingans[i].nomorInduk;
    var nama = bimbingans[i].nama;

    bimbingansResults.innerHTML += '<div class="well">'+
                                  '<h3>'+nomorInduk+
                                   nama+
                                  ' <a onclick="deletebBimbingan(\''+nama+'\')" class="btn btn-danger" href="#">Delete</a> ' +
                                  '</h3>'+
                                  '</div>';
  }
}

// Validate Form
function validateForm(nomorInduk, nama, kelas, jurusan, judul, tgl, jam, email, materi){
  if(!nomorInduk || !nama ||!kelas || !judul || !jurusan || !tgl|| !jam || !email || !materi){
    alert('Please fill in the form');
    return false;
  }

}
}